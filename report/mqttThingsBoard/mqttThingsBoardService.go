package mqttThingsBoard

import (
	"encoding/json"
	"goAdapter/device"
	"goAdapter/setting"
)

type MQTTThingsBoardInvokeServiceAckParamTemplate struct {
	ClientID  string `json:"clientID"`
	CmdName   string `json:"cmdName"`
	CmdStatus int    `json:"cmdStatus"`
}

type MQTTThingsBoardInvokeServiceAckTemplate struct {
	Method string                                         `json:"method"`
	Params []MQTTThingsBoardInvokeServiceAckParamTemplate `json:"params"`
}

type MQTTThingsBoardInvokeServiceRequestParamTemplate struct {
	ClientID  string                 `json:"clientID"`
	CmdName   string                 `json:"cmdName"`
	CmdParams map[string]interface{} `json:"cmdParams"`
}

type MQTTThingsBoardInvokeServiceRequestTemplate struct {
	Method    string                                             `json:"method"`
	RequestID string                                             `json:"requestID"`
	Params    []MQTTThingsBoardInvokeServiceRequestParamTemplate `json:"params"`
}

func (r *ReportServiceParamThingsBoardTemplate) ReportServiceThingsBoardInvokeServiceAck(reqFrame MQTTThingsBoardInvokeServiceRequestTemplate, code int, ackParams []MQTTThingsBoardInvokeServiceAckParamTemplate) {

	ackFrame := MQTTThingsBoardInvokeServiceAckTemplate{
		Method: reqFrame.Method,
		Params: ackParams,
	}

	sJson, _ := json.Marshal(ackFrame)
	serviceInvokeTopic := "v1/devices/me/rpc/response/" + reqFrame.RequestID

	setting.ZAPS.Infof("rpc_reply topic: %s", serviceInvokeTopic)
	setting.ZAPS.Debugf("rpc_reply: %v", string(sJson))
	if r.GWParam.MQTTClient != nil {
		token := r.GWParam.MQTTClient.Publish(serviceInvokeTopic, 0, false, sJson)
		token.Wait()
	}
}

func (r *ReportServiceParamThingsBoardTemplate) ReportServiceThingsBoardProcessInvokeService(reqFrame MQTTThingsBoardInvokeServiceRequestTemplate) {

	ReadStatus := false

	ackParams := make([]MQTTThingsBoardInvokeServiceAckParamTemplate, 0)

	for _, v := range reqFrame.Params {
		for _, node := range r.NodeList {
			if v.ClientID == node.Param.ClientID {
				//从上报节点中找到相应节点
				coll, ok := device.CollectInterfaceMap.Coll[node.CollInterfaceName]
				if !ok {
					continue
				}

				for _, n := range coll.DeviceNodeMap {
					if n.Name == node.Name {
						//从采集服务中找到相应节点
						cmd := device.CommunicationCmdTemplate{}
						cmd.CollInterfaceName = node.CollInterfaceName
						cmd.DeviceName = node.Name
						cmd.FunName = v.CmdName
						paramStr, _ := json.Marshal(v.CmdParams)
						cmd.FunPara = string(paramStr)
						ackParam := MQTTThingsBoardInvokeServiceAckParamTemplate{
							ClientID: node.Param.ClientID,
							CmdName:  v.CmdName,
						}

						ackData := coll.CommQueueManage.CommunicationManageAddEmergency(cmd)
						if ackData.Status {
							ReadStatus = true
							ackParam.CmdStatus = 0
						} else {
							ReadStatus = false
							ackParam.CmdStatus = 1
						}
						ackParams = append(ackParams, ackParam)
					}
				}
			}
		}
	}

	if ReadStatus == true {
		r.ReportServiceThingsBoardInvokeServiceAck(reqFrame, 0, ackParams)
	} else {
		r.ReportServiceThingsBoardInvokeServiceAck(reqFrame, 1, ackParams)
	}
}
