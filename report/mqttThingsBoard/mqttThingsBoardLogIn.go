package mqttThingsBoard

import (
	"encoding/json"
	"goAdapter/setting"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

type MQTTNodeLoginTemplate struct {
	Device string `json:"device"`
}

func MQTTThingsBoardOnConnectHandler(client MQTT.Client) {
	setting.ZAPS.Debug("MQTTThingsBoard 链接成功")
}

func MQTTThingsBoardConnectionLostHandler(client MQTT.Client, err error) {
	setting.ZAPS.Debug("MQTTThingsBoard 链接断开")
}

func MQTTThingsBoardReconnectHandler(client MQTT.Client, opt *MQTT.ClientOptions) {
	setting.ZAPS.Debug("MQTTThingsBoard 链接重新链接")
}

func MQTTThingsBoardGWLogin(param ReportServiceGWParamThingsBoardTemplate, publishHandler MQTT.MessageHandler) (bool, MQTT.Client) {

	opts := MQTT.NewClientOptions().AddBroker(param.IP + ":" + param.Port)

	opts.SetClientID(param.Param.ClientID)
	opts.SetUsername(param.Param.UserName)
	//hs256 := sha256.New()
	//hs256.Write([]byte("zhsHrx123456@"))
	//password := hs256.Sum(nil)
	//param.Password = string(hex.EncodeToString(password))
	//setting.ZAPS.Debugf("ThingsBoard password %v", param.Password)
	//param.Password = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InpocyIsImNsaWVudGlkIjoiIiwiaWF0IjoxNjI2NzAwMzE1fQ.EICw6uVoP-_X2iKcdkmBTJevFspm7Nz9ipHjJpr8eHg"
	opts.SetPassword(param.Param.Password)
	opts.SetKeepAlive(30 * time.Second)
	opts.SetDefaultPublishHandler(publishHandler)
	opts.SetAutoReconnect(false)
	opts.SetOnConnectHandler(MQTTThingsBoardOnConnectHandler)
	opts.SetConnectionLostHandler(MQTTThingsBoardConnectionLostHandler)
	opts.SetReconnectingHandler(MQTTThingsBoardReconnectHandler)

	// create and start a client using the above ClientOptions
	mqttClient := MQTT.NewClient(opts)
	if token := mqttClient.Connect(); token.Wait() && token.Error() != nil {
		setting.ZAPS.Errorf("上报服务[%s]链接ThingsBoard Broker失败 %v", param.ServiceName, token.Error())
		return false, nil
	}
	setting.ZAPS.Infof("上报服务[%s]链接ThingsBoard Broker成功", param.ServiceName)

	subTopic := ""
	//订阅属性下发请求
	subTopic = "v1/gateway/attributes"
	MQTTThingsBoardSubscribeTopic(mqttClient, subTopic)

	//订阅RPC下发请求
	subTopic = "v1/devices/me/rpc/request/+"
	MQTTThingsBoardSubscribeTopic(mqttClient, subTopic)

	return true, mqttClient

}

func MQTTThingsBoardSubscribeTopic(client MQTT.Client, topic string) {

	if token := client.Subscribe(topic, 0, nil); token.Wait() && token.Error() != nil {
		setting.ZAPS.Warnf("ThingsBoard上报服务订阅主题%s失败 %v", topic, token.Error())
		return
	}
	setting.ZAPS.Infof("ThingsBoard上报服务订阅主题%s成功", topic)
}

func (r *ReportServiceParamThingsBoardTemplate) GWLogin() bool {
	status := false
	status, r.GWParam.MQTTClient = MQTTThingsBoardGWLogin(r.GWParam, ReceiveMessageHandler)
	if status == true {
		r.GWParam.ReportStatus = "onLine"
	}

	return status
}

func MQTTThingsBoardNodeLoginIn(param ReportServiceGWParamThingsBoardTemplate, nodeMap []string) int {

	nodeLogin := MQTTNodeLoginTemplate{}

	for _, v := range nodeMap {
		nodeLogin.Device = v

		sJson, _ := json.Marshal(nodeLogin)
		loginTopic := "v1/gateway/connect"

		setting.ZAPS.Infof("上报服务[%s]发布节点上线消息主题%s", param.ServiceName, loginTopic)
		setting.ZAPS.Debugf("上报服务[%s]发布节点上线消息内容%s", param.ServiceName, sJson)

		if param.MQTTClient != nil {
			token := param.MQTTClient.Publish(loginTopic, 0, false, sJson)
			token.Wait()
		}
	}
	return 0
}

func (r *ReportServiceParamThingsBoardTemplate) NodeLogIn(name []string) bool {

	nodeMap := make([]string, 0)
	status := false

	setting.ZAPS.Debugf("上报服务[%s]节点%s上线", r.GWParam.ServiceName, name)
	for _, d := range name {
		for _, v := range r.NodeList {
			if d == v.Name {
				nodeMap = append(nodeMap, v.Param.ClientID)
				MQTTThingsBoardNodeLoginIn(r.GWParam, nodeMap)
			}
		}
	}

	return status
}
